/**
 * Master application
 * Includes Classes Env, App
 */

window.App = window.App || {};
window.Env = window.Env || {};


(function(App, Env, API){

    var UI = {},
        User = {};

    App.init = function initializeApplication () {
        User.init();
        this.changeStates();
        UI.init();
    };

    App.state = 'default';

    App.states = {
        'default': function notLoggedInUser () {
            //bind UI events
            if(App.state == 'default') {
                UI.elements.registerSection.show();
                UI.elements.registerSectionButton.on("click", function(e) {

                    e.preventDefault();
                    registerUser(UI.elements.usernameField[0].value, UI.elements.passwordField[0].value, UI.elements.passwordField2[0].value);
                });
            } else if(UI.currentForm == 'login'){
                UI.elements.loginSection.show();
                e.preventDefault();
                registerUser(UI.elements.usernameField, UI.elements.passwordField, UI.elements.PasswordField2);
            }
        },

        'viewDocuments': function viewUserDocuments () {
            //bind UI events
        }
    };

    App.getCurrentState = function getAppCurrentState () {

       return User.loggedIn ? 'viewDocuments' : 'default';
    };

    App.changeStates = function changeAppState() {
        var state = this.getCurrentState();
        if(this.state !== state) {
            UI.events.unbind[this.state]();
        }

        this.state = state;

        UI.events.bind[state]();
        App.states[state]();
    };

    App.callBacks = {
        'login': function loginCallBack (result) {

        },

        'register': function registerCallBack (result) {

        }
    };

    /**
     * User Object
     */

    User = {

        settings: {
            userName: null,
            userId: null
        },

        init: function initializeUser () {
            this.settings = this.status.get();
            this.loggedIn = this.settings.userName && this.settings.userId ? true : false;
        },

        status: {

            /**
             * Set user name and id
             * @param [String] userName
             * @param [String] id
             * returns void
             */
            set: function (userName, userId) {
                localStorage.userName = userName;
                localStorage.userId = userId;

                User.loggedIn = userName && userId ? true : false;
            },

            /**
             * Set user name and id
             * returns [Object] - userName & userId
             */
            get: function () {

                return {
                    userName : localStorage.userName,
                    userId   : localStorage.userId,
                }
            }
        },

        loggedIn: false
    };

    UI.elements = {};

    UI.init = function initializeUI () {
        UI.elements.usernameField = $( '#username'),
        UI.elements.passwordField  = $('#password'),
        UI.elements.passwordField2 = $('#password2'),
        UI.elements.usernameLoginField  = $('#usernameLogin'),
        UI.elements.passwordLoginField  = $('#passwordLogin'),

        UI.elements.registerSection = $( "#register" ),
        UI.elements.registerSectionButton = $( "#register button" ),
        UI.elements.loginSection = $( "#login" ),
        UI.elements.loginSectionButton = $( "#login button" ),
        UI.elements.navigation = $('.navbar-nav li');

        this.events.bind['default']();
    };

    //changed on click on the tab
    UI.currentForm = 'register';

    UI.events = {
        bind: {
            'default': function bindDefaultUIEvents () {

                if(UI.currentForm == 'register') {
                    UI.elements.registerSection.show();
                    UI.elements.loginSection.hide();
                } else if(UI.currentForm == 'login') {
                    UI.elements.registerSection.hide();
                    UI.elements.loginSection.show();
                }

                UI.elements.navigation.on('click', function () {
                    if($(this).hasClass("active")) {
                        return;
                    } else {
                        var showForm = $(this).attr('data-form');

                        $(this).addClass('active')
                            .siblings('li').removeClass('active');

                        $('#forms').find('#' + showForm).show()
                                .siblings('.col-xs-6').hide();
                    }
                });
            },
            'viewDocuments': function bindViewDocumentsEvents () {

            }
        },

        unbind: {
            'default': function bindDefaultUIEvents () {

            },
            'viewDocuments': function bindViewDocumentsEvents () {

            }
        }
    };

    /***
     * Check for registered user
     * @param {String} userName
     * @param {String} userPass
     * returns void
     */
    function checkUser (userName, userPassword) {
        var dummyCallback = function(result){
            console.log(result);
            return result;
        };

        API.user.check(userName, userPassword, dummyCallback);
    }

    App.register = function registerUser (username, password, passwordRepeat) {

        // After SUCCSESS set USER constructor
        if (password === passwordRepeat) {

            checkUser(username, password);
                console.log(result);

            if(result) {
                API.user.add(username, password);
                User.status.set(username, password);
                UI.currentForm = 'login';
                App.callBacks['register'](result);

                UI.init();
            }

        }
    };

    function loginUser (userField, passField) {

        // After SUCCSESS set USER constructor
        // body...

        var username = 'Viktor' || userField.value;
        var password = 'passss' || passField.value;

        console.log(username, password);

    }

    /**
     * Document ready
     */
    $(function() {

        App.init();
    });

})(App, Env, API);